# Schaltjahr berechnen

> Als Schaltjahr (lateinisch *annus intercalarius* oder *annus bissextus*) wird in der Kalenderrechnung ein Jahr bezeichnet, das im Unterschied zum Gemeinjahr einen zusätzlichen Schalttag oder Schaltmonat enthält.
> *Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Schaltjahr)*

Erstellen Sie ein Programm, welches für eine gegebene Jahreszahl berechnen kann, ob es sich bei diesem Jahr um ein Schaltjahr handelt.

## Regeln für die Berechnung von Schaltjahren

Für die Berechnung von Schaltjahren können Sie die nachfolgenden Regeln verwenden:

- Schaltjahre sind Jahre, deren Jahreszahl durch 4 teilbar ist.
- Der 29. Februar fehlt in Jahren, welche durch 100 teilbar sind.
- Jahre, welche durch 400 teilbar sind, sind Schaltjahre.

## Beispiele

Die folgenden Beispiele illustrieren die Anwendung der oben genannten Regeln:

- 2004 ist ein Schaltjahr (2004 ist durch 4 teilbar)
- 1900 ist kein Schaltjahr (1900 ist durch 100 teilbar)
- 2000 ist ein Schaltjahr (2000 ist durch 400 teilbar)

## Varianten

- Ausgehend von einer Jahreszahl wird das nächste Schaltjahr berechnet.
- Ausgehend von einer Jahreszahl werden die nächsten *x* Schaltjahre berechnet.
- Ausgehend von einer Jahreszahl wir das letzte Schaltjahr oder die letzten *x* Schaltjahre berechnet.
- Die für die Berechnung relevanten Daten (*Jahreszahl*, *Anzahl Jahre*) können beim Programm zur Laufzeit erfasst (eingegeben) werden.
- Die Berechnung kann (ohne erneuten Programmstart) beliebig oft wiederholt werden.