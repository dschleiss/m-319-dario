# Teilerprüfung

Erstellen Sie ein Programm, welches prüft, ob eine Zahl `n` durch eine andere Zahl `t` teilbar ist. 
Oder umgekehrt: Prüfen Sie, ob die Zahl `t` ein Teiler der Zahl `n` ist.

## Beispiele

- `n = 21` und `t = 7` → Ergebnis: `true`
- `n = 21` und `t = 5` → Ergebnis: `false`
- `n = 163` und `t = 13` → Ergebnis: `true`
  
## Varianten

- Erstellen Sie ein Programm, welches alle Teiler einer Zahl berechnet.
- Erstellen Sie ein Programm, welches prüft ob eine beliebige Zahl `p` ein [Primzahl](https://de.wikipedia.org/wiki/Primzahl) ist.