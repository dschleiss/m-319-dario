# Geheimzahl raten

Erstellen Sie ein Programm, bei welchem Spielerinnen und Spieler eine vom Computer generierte "Geheimzahl" erraten müssen. Nach jedem abgegebenen Tipp, erhält die Spielerin bzw. der Spieler einen Hinweis, ob die Geheimzahl erraten wurde, grösser als der abgegebene Tipp ist oder kleiner als der abgegebene Tipp ist.

Für das Generieren einer Zufallszahl können Sie einen Zufallszahlengenerator verwenden:

```c#
// Define random number generator
Random numberGenerator = new Random();

// Generate a new, non-negative random number
int randomNumber1 = numberGenerator.Next();

// Generate a new, non-negative random number less than 42
int randomNumber2 = numberGenerator.Next(42);

// Generate a new random number between 10 (inclusive) and 21 (exclusive)
int randomNumber3 = numberGenerator.Next(10, 21);
```

*Die genaue Funktionsweise des Zufallszahlengenerators (Klasse [Random](https://docs.microsoft.com/en-us/dotnet/api/system.random?view=net-5.0)) erfordert Kenntnisse in der objektorientierten Programmierung. Diese Programmierparadigma wird im Modul 320 eingehend behandelt.*

## Beispiel

Das nachfolgende Beispiel zeigt eine mögliche Interaktion:

- *Generierte Geheimzahl: 42*
- Ausgabe: `Errate die Geheimzahl zwischen 0 und 100:`
- Eingabe: `18`
- Ausgabe: `Die Geheimzahl ist grösser als 18. Versuch's nochmals...`
- Eingabe: `33`
- Ausgabe: `Die Geheimzahl ist grösser als 33. Versuch's nochmals...`
- Eingabe: `50`
- Ausgabe: `Die Geheimzahl ist kleiner als 50. Versuch's nochmals...`
- Eingabe: `42`
- Ausgabe: `GLÜCKWUNSCH! Die gesuchte Geheimzahl ist 42.`
- *Das Programm wird beendet.*

## Varianten

- Der Bereich für die Generierung der Geheimzahl kann von der Spielerin oder vom Spieler definiert (d.h. eingegeben) werden.
- Die Hinweise bei falschen Tipps sind in Abhängigkeit der Abweichung von der Geheimzahl präziser (z.B. `Die Geheimzahl ist VIEL grösser` vs. `Die Geheimzahl ist grösser`)
- Mehrspielermodus
  - Die Geheimzahl wird nicht vom Computer generiert sondern von einer anderen Spielerin oder einem anderen Spieler eingegeben.
  - Beide Spieler/-innen geben eine Geheimzahl ein und raten dann abwechseln. Welche/-r Spieler/-in kann die Geheimzahl schneller erraten?