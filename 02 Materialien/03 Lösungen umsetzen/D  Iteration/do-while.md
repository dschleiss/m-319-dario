# `do-while`-Iteration

Bei der `do-while`-Iteration werden Anweisungen so lange wiederholt, wie eine *Bedingung* erfüllt ist. Sobald die Bedingung *nicht mehr erfüllt* ist, wird die Ausführung der Schleife abgebrochen. Dabei wird die Bedingung jeweils *nach der Ausführung* des Blocks (Code, der zwischen `{` und `}` steht) überprüft. Die Anweisungen im Schleifenkörper werden also *mindestens ein Mal* ausgeführt.

```c#
do
{
    Anweisungen
} while (Bedingung);
```

Die Bedingung wird nach dem Block mit den Anweisungen und nach dem `while` Schlüsselwort zwischen runde Klammern geschrieben. Sie ist ein Ausdruck, welcher zu einem boolschen Wert (`true` oder `false`) evaluieren muss. Typischerweise wird im Inneren der `do-while`-Schleife eine Variable so verändert, dass die Bedingung in Abhängigkeit dieser Variable entweder *erfüllt* oder *nicht erfüllt* ist.

Wenn der zu wiederholende Code-Block aus nur einer einzigen Anweisung besteht, kann diese theoretisch ohne die geschweiften Klammern, direkt auf der Zeile unterhalb des Schlüsselwortes `do` geschrieben werden. Von dieser Notation wird jedoch abgeraten, da sie gerade bei Programmieranfängerinnen und -anfängern schneller zu unübersichtlichen Code-Stellen oder Fehlern führen kann.

```c#
// This is necessary for the Console.WriteLine statement
// at the end of this code snippet.
using System;

// Save the current year in the variable named "year".
int year = DateTime.Now.Year;

// Do-while loop to calculate the next leap year.
// Note: The actual calculation of a leap year is not
// as trivial as shown in the while condition below!
do
{
    year++;
} while (year % 4 != 0);

// Print the result of the calculation to the console.
Console.WriteLine($"Next leap year is: {year}");
```
