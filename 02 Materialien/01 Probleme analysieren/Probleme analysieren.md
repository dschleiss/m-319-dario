# Probleme analysieren

In jedem Beruf lösen Fachfrauen und Fachmänner berufsspezifische *Probleme*, welche durch Kundinnen und Auftraggeber an sie herangetragen werden. In der Informatik und im Bereich der Applikationsentwicklung im Speziellen, sind diese Probleme meist Aufgaben, Prozesse oder Dienstleistungen, für deren einfachere Bearbeitung *Softwarelösungen* entwickelt, umgesetzt, getestet, eingeführt, betrieben und unterhalten werden sollen.

Um effizient und effektiv die "richtige Software" entwickeln zu können, ist es wichtig, dass die Softwareentwicklerinnen und Softwareentwickler genau verstehen, was die Auftraggeberinnen und Auftraggeber möchten. Diese scheinbar einfache Aufgabe ist in der Praxis meist schwieriger als anfänglich vermutet und hat bereits bei unzähligen Projekten zu massiven Kostenüberschreibungen und/oder gar zum Misslingen eines Projekts geführt.

Neben einer gemeinsamen Sprache für alle Beteiligten am Projekt und einem sauberen Projektmanagement ist die anfängliche Identifikation und Dokumentation aller Anforderungen eine wesentliche Gelingensbedingung für ein Softwareprojekt. Das Erheben, Formulieren, Dokumentieren und Überprüfen von Anforderungen ist eine eigenständige Disziplin ([Requirement Engineering](https://www.jobscout24.ch/de/jobs/requirements%20engineer/)) im grossen Feld der Softwareentwicklung.

## Problemraum und Lösungsraum

Um die Anforderungen für eine Software aufnehmen und dokumentieren zu können, ist es dringend notwendig, die Wünsche, Bedürfnisse, Probleme und sonstigen Rahmenbedingungen der Auftraggebenden zu kennen. Da diese jedoch selber meist keine Softwareentwicklerinnen oder Applikationsentwickler, sondern Expertinnen und Experten in ihrem jeweiligen Fachbereich sind, kann von dieser Seite keine vertiefte Kenntnis im Bereich des *Requirement Engineerins* vorausgesetzt werden. Es ist daher die Aufgabe der Applikationsentwicklerinnen und Applikationsentwickler, mit geeigneten Methoden und Werkzeugen ein möglichst vollständiges Bild aller Anforderungen zu zeichnen.

Laien haben bei der Formulierung von Anforderungen meist Schwierigkeiten, die *Probleme* von den *Lösungen* zu trennen. Dabei Formulieren Auftraggebende die Anforderung an eine Software bereits so, dass die Anforderugen einen Teil der Lösung vorwegnehmen - das *Problem* wir also mit der *Lösung* vermischt. Meist steckt dabei nur *gute Absicht* der Auftraggebenden dahinter - diese wollen den Entwicklerinnen und Entwicklern möglichst genau beschreiben, was sie vom fertigen Produkt erwarten. Dass dabei jedoch alternative Lösungen und Varianten verloren gehen können, wird meist nicht bedacht.

**Es ist daher dringend erforderlich, dass das zu bearbeitende Problem in einem ersten Schritt *lösungsfrei* betrachtet wird.** Dabei wird nur das Problem und dessen Kontext analysiert. Mögliche Lösungen werden ganz bewusst vernachlässigt, weil diese die präszise und neutrale Analyse des Problems möglicherweise erschweren oder einschränken könnten.

### Problemraum

Im Problemraum stehen die Nutzerinnen und Nutzer der später zu entwickelnden Lösung sowie das Problem ansich im Zentrum. Es geht dabei einzig darum, die "Aufgabenstellung" (= das Problem) sowie die Personen, welche an der Lösung für die gestellte Aufgabe interessiert sind, besser zu verstehen. Alter, Vorkenntnisse, Bedürfnisse, Aufgaben, Zuständigkeiten, Verantwortlichkeiten, Vorlieben, und viele andere Aspekte der nutzenden Personen könnten unterschiedlichste Einflüsse auf die später zu entwicklende Software haben. Zudem muss auch der Problemgegenstand genau verstanden werden. Ganz egal, ob es sich um ein Problem aus dem Finanzbrache, aus der Agrarindustrie, aus dem Bildungssektor oder aus der Maschinenindustrie handelt - Softwareentwicklerinnen und Softwareentwickler sollten die Prozesse, Begriffe und Rahmenbedingungen möglichst gut verstehen, um in einem nachfolgenden Schritt die beste Lösung für das analysierte Problem und die nutzenden Personen entwickeln zu können.

Oder kurz ausgedrückt: Wer eine Lösung finden will, muss sich zuerst ausführlich über das Problem informieren. In diesem Kontext gehören auch die Nutzerinnen und Nutzer der zu entwicklenden Lösung zum Problemraum.

### Lösungsraum

Der Lösungsraum ist ein "imaginärer Ort", an welchem viele unterschiedliche Lösungsansätze entwickelt und erprobt werden. Dabei gilt zu Beginn: Je mehr desto besser! Es sollen möglichst viele unterschiedliche Ideen und Lösungsansätze gesammelt werden. Diese müssen noch nicht völlig ausgereift sein. Vorallem sollten Einschränkungen wie Zeit oder Budget in dieser frühen Phase der Lösungsfinden noch nicht berücksichtigt werden.

Nachdem eine breite Ideensammlung entstanden ist, sollen einige interessant und/oder realistisch erscheinende Ansätze genauer betrachtet, ausgearbeitet und möglicherweise erprobt werden. Dafür eignen sich sogenannte *Prototypen*, welche wesentliche Aspekte der Lösung vereinfacht und schematisch erlebbar machen. Dadurch sollen spätere Nutzende miteingebunden werden um möglichst früh Feedback zum jeweiligen Lösungsansatz zu erhalten.