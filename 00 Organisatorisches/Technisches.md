# Technische Informationen

In diesem Dokument werden Informationen, Anleitungen, Hilfestellungen, Tipps, Links und weitere Materialien rund um die technische Infrastruktur und die persönliche Arbeitsumgebung der Lernenden gesammelt.

## Programmiersprache

In der Modulidentifikation des Modul 319 ist keine Programmiersprache explizit erähnt. Ebenso ist kein Programmierparadigma aufgeführt. Grundsätzlich ist die Modulidentifikation also komplett sprach- und technologieneutral.

Um das Modul 319 bestmöglich in die schulische (und betriebliche) Ausbildung zu integrieren, bzw. um mit dem Modul 319 die bestmögliche Grundlage für die weiteren Module zu legen, wird explizit die Anendung der **Programmiersprache C#** vorgegeben.

### Top Level Statements

C# ist eine Programmiersprache, welche sich grundsätzlich durch die Anwendung des objektorientierten Programmierparadigmas auszeichnet. Seit der Version 9.0 von C# können jedoch auch sogenannte *Top Level Statements* geschrieben werden, welche die Programmierung in C# komplett ohne Klassen und Methoden erlauben.

Dazu wird der Programmcode in *einer* Datei des Projekts direkt ab der ersten Zeile geschrieben. Es ist kein *Namespace*, keine *Klasse* und keine *`Main`-Methode* notwendig, um das Programm auszuführen.

### Sprachspezifische Syntax vs. allgemeine Programmierkonzepte

Im Modul 319 sollen grundsätzlich *allgemeine Programmierkonzepte* vermittelt werden, welche für (fast) alle modernen Programmiersprachen Gültigkeit und Relevanz haben. Dennoch ist es gerade für Programmieranfängerinnen und -anfänger einfacher, zuerst auf eine einzige, konkrete Sprache zu fokussieren um damit Erfahrungen zu sammeln. Dennoch soll dabei bedenkt werden, dass sämtliche Inhalte des Moduls 319 grundsätzlich sprachneutral betrachtet werden können. Ebenso ist die Übertragbarkeit der Inhalte in viele andere Programmiersprachen wie beispielsweise Java, PHP, Python, die ganze C-Familie (C, C++), Swift, etc. mit geringen, syntaktischen Anpassungen ziemlich einfach möglich.

## Visual Studio Code

Als *Integrierte Entwicklungsumgebung* (englisch: *Integrated Development Environment*, *IDE*) wird im Unterricht des Moduls 319 vorwiegend [Visual Studio Code](https://code.visualstudio.com) eingesetzt. Dies ist eine moderne, frei verfügbare IDE, welche auf vielen verschiedenen Plattformen lauffhähig ist und dank vieler, modularer Plug-Ins für die Programmierung mit vielen verschiedenen Programmiersprachen geeignet ist.

Es steht den Lernenden grundsätzlich frei, welche IDE gewählt und eingesetzt wird. Im Unterricht am GIBZ wird Wert darauf gelegt, dass die eingesetzten Tools für die professionelle Arbeit als Informatikerin bzw. Informatiker geeignet sind und dass die Lernenden in Eigenverantwortung ihr Set an Werkezugen zum Lernen und Arbeiten pflegen.

### Empfohlenes Plugin: C#

Um in Visual Studio Code Programme in C# effizient entwickeln zu können, ist die Installation des Plugins mit dem Titel [C# for Visual Studio Code (powered by OmniSharp)](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp) dringend empfohlen.


## Neues Projekt erstellen

Um in C# ein neues Projekt zu erstellen, kann die nachfolgende Anleitung genutzt werden:

1. Visual Studio Code öffnen
2. Im *Welcome*-Fenster mit der Option `Open...` ein neues Verzeichnis öffnen
3. Das Verzeichnis, in welchem das Projekt angelegt werden soll, erstellen bzw. auswählen
4. Das *Terminal* für die Eingabe von Befehlen öffnen (`Terminal` --> `New Terminal`)
5. Den Befehl `dotnet new console` eingeben, um ein neues Konsolenprojekt zu erstellen. Standardmässig heisst das Projekt gleich wie der Verzeichnis, in welchem das Projekt angelegt wird. Alternativ kann der Projektname mit der zusätzlichen Option `--name Projektname` angegeben werden.
6. Für das Ausführen und Debuggen des Codes müssen nun noch verschiedene Dateien zum Projekt hinzugefügt werden. Dazu kann *eine* der nachfolgenden Optionen genutzt werden:
   1. Möglicherweise erscheint eine *Notification*, dass die entsprechenden Assets zum Projekt hinzugefügt werden können. Bestätigen Sie diese Meldung mit einem Klick auf `Yes`.
   2. Sie können das Hinzufügen der Assets auch über die *Command Palette* (`F1`) und den Befehl `.NET: Generate Assets for Build and Debug` ausführen.

Im neuen Projekt finden Sie eine Datei mit dem Namen `Program.cs`. Diese enthält beispielhaft einige Zeilen Code. Sie können diesen Code komplett löschen und in der nun leeren `Program.cs`-Datei Ihren eigenen Programmcode schreiben.

## Programm ausführen

Um ein Programm auszufühen, können Sie in der *Activity Bar* (Bereich ganz links) auf das Icon für *Run and Debug* klicken. Danach finden Sie am oberen Rand des *Panel* ein grünes "Play-Icon" um die Ausführung des Programms zu starten.

Alternativ können Sie die Ausführung des Programms auch über die Taste `F5` starten.

## Dateneingaben verarbeiten

Wenn Sie ein Programm ausführen, welches Eingaben auf dem Terminal erwartet, müssen Sie in Visual Studio Code zwei zusätzliche Änderungen in der Datei `.vscode/launch.json` vornehmen:

1. Ändern Sie den Wert für `console` zu `integratedTerminal`.
2. Fügen Sie einen zusätzlichen Eintrag mit dem Schlüssel `internalConsoleOptions` sowie dem Wert `neverOpen` hinzu.

Die neue Konfiguration sieht dann ähnlich aus, wie im nachfolgenden, verkürzten Beispiel:

```json
"version": "0.2.0",
"configurations": [
    {
        // Most object entries omitted...
        
        "console": "integratedTerminal",
        "internalConsoleOptions": "neverOpen",

        // Maybe some other entries here...
    }
]
```