# Input: Rekursive Funktionen

Rekursive Funktionen sind Funktionen, welche sich selber aufrufen. Funktionen (oder Methoden), welche *andere* Methoden aufrufen, sind keine Seltenheit. Im Kontext der objektorientierten Programmierung ist dies grundsätzlich sogar der Normalfall. Dort beginnt die Programmausführung mit dem Aufruf der `Main`-Methode. Innerhalb dieser `Main`-Methode werden weitere Methoden aufgerufen, welche wiederum andere Methoden aufrufen. Und so weiter...

Ein rekursiver Aufruf ist daran erkennbar, dass innerhalb einer Funktion ein Aufruf jener Funktion stattfindet, in welcher der Aufruf steht. Das nachfolgende, abstrakte Code-Beispiel illustiert diese Situation:

```c#
// Define a recursive function
void RecursiveFunction(int number)
{
    System.Console.WriteLine(number);
    RecursiveFunction(number + 1);
}

// Call recursive function
RecursiveFunction(0);   // 0, 1, 2, 3, 4, 5, ... (endless)
```

Es gilt dabei zu beachten, dass eine *endlose* Fortsetzung der rekursiven Funktionsaufrufe - wie im oben stehenden Beispiel - meist nicht beabsichtigt sind. Dies würde ziemlich schnell zu einem *Stack Overflow* und damit zu einem Programmabsturz führen. Der rekursive Funktionsaufruf ist daher meist abhängig von einer Bedingung.

## Beispiel: Fibonaccizahlen
Ein häufig verwendetes Beispiel zur Illustration von rekursiven Funktionen ist die Generierung von Fibonaccizahlen. Dies kann entweder mit Iteration (Schleifen) oder durch rekursive Funktionsaufrufe erfolgen. Der letztgenannte Anwendungsfall wird im nachfolgenden Code-Beispiel dargestellt:

```c#
int Fibonacci(int position)
 {
    if (position <= 2)
    {
        return 1;
    }
    else
    {
        return Fibonacci(position - 1) + Fibonacci(position-2);
    }
 }

 System.Console.WriteLine("Position  1: {0,4}", Fibonacci(1));  // Position  1:    1
 System.Console.WriteLine("Position  2: {0,4}", Fibonacci(2));  // Position  2:    1
 System.Console.WriteLine("Position  3: {0,4}", Fibonacci(3));  // Position  3:    2
 System.Console.WriteLine("Position  8: {0,4}", Fibonacci(8));  // Position  8:   21
 System.Console.WriteLine("Position 11: {0,4}", Fibonacci(11)); // Position 11:   89
 System.Console.WriteLine("Position 16: {0,4}", Fibonacci(16)); // Position 16:  987
```

## Beispiel: Fakultät
Die Fakultät einer natürlichen Zahl ist das Produkt aller natürlichen Zahlen, ohne Null, welche kleiner oder gleich jener natürlichen Zahl sind. In der Mathematik wird die Fakultät mit einem Ausrufezeichen hinter dem Operanden dargestellt.

Beispiel: $`5! = 5 \cdot 4 \cdot 3 \cdot 2 \cdot 1 = 120`$

In C# kann die Berechnung der Fakultät mit einer rekursiven Funktion implementiert werden:

```c#
int Faculty(int number)
{
    if (number <= 1)
    {
        return 1;
    }
    else
    {
        return Faculty(number - 1) * number;
    }
}

System.Console.WriteLine(Faculty(0));   //      1
System.Console.WriteLine(Faculty(1));   //      1
System.Console.WriteLine(Faculty(5));   //    120
System.Console.WriteLine(Faculty(9));   // 362880
```