# Arbeit mit dem Kompetenzenraster

Das [Kompetenzenraster](Kompetenzenraster.md) bietet einen Überblick über alle relevanten Kompetenzen, welche im Rahmen des Unterrichts im Modul 319 erreicht werden sollen. Dabei soll das Kompetenzenraster *nicht* als Arbeitsinstrument verstanden werden, welches "von oben links nach unten rechts" abgearbeitet werden soll. Vielmehr erhalten die Lernenden mit dem Kompentenzenraster ein Werkzeug, um ihr Lernen selber zu organisieren und zu reflektieren.

Ein Wechseln zwischen den verschiedenen Kompetenzbereichen ist durchaus möglich und sogar empfohlen. Theoretisch ist es wohl möglich, die einzelnen Kompetenzbereiche rein sequentiell, beginnend mit der Stufe *Beginner* bis hin zur Stufe *Expert* zu bearbeiten. Dabei entsteht jedoch die Gefahr, dass verschiedene Kompetenzen isoliert und ohne die notwendigen Verknüpfungen mit anderen Themen bearbeitet werden. Emfophlen wird daher, dass in allen vier Kompetenzbereichen sämtliche Kompetenzen auf den Stufen *Beginner* und *Intermediate* erworben werden, bevor in einem Kompetenzbereich die Vertiefung auf den Stufen *Advanced* oder gar *Expert* erfolgt.

> Es gibt keine vordefinierte Reihenfolge zur Bearbeitung der verschiedenen Themen und Inhalte. Lernende planen und reflektieren ihren persönlichen Lernweg mithilfe des Kompetenzenrasters selbständig.

## Kompetenzen nachweisen

Das Kompetenzenraster stellt ein grosses "Spielfeld" dar, auf welchem sich die Lernenden bewegen können. Um dabei den Erwerb der verschiedenen Kompetenzen und den damit verbundenen  Lernprozess besser reflektieren und steuern zu können, soll der Erwerb der Kompetenzen regelmässig nachgewiesen werden. Dadurch erhalten die Lernenden regelmässig Feedbacks, inwiefern die bearbeiteten Kompetenzen erreicht wurden und wo noch weitere Aufarbeitungen oder Vertiefungen notwendig sind.

> Kompetenzen sollen fortlaufend nachgewiesen werden, sobald diese in ausreichendem Mass vorhanden sind.

Durch den fortlaufenden Nachweis der erreichten Kompetenzen erhält die Lehrperson zudem ein gutes Instrument um weitere Unterlagen, Inputs, Aufgaben oder Hilfestellungen anzubieten. Auf diese Weise ist es möglich, den individuellen Lernprozess aller Lernenden ab den ersten Stunden zu begleiten und Lernende individuell zu unterstützen, fördern und fordern.

### Methoden zum Nachweis erworbener Kompetenzen

Die Form des Kompetenznachweis ist offen. Es steht den Lernenden grundsätzlich frei, in welcher Form sie erworbene Kompetenzen nachweisen möchten. Im Vergleich zu klassischen Prüfungsformen erfordert diese Art des Kompetenznachweis zusätzlichen Aufwand von den Lernenden. Es ist nicht mehr die Lehrperson, welche die Form definiert und beispielsweise Prüfungsfragen oder -aufgaben ausarbeitet, welche von allen Lernenden gelöst werden sollen. Die Lernenden wählen selbständig und individuell die passende Form, um ihre Kompetenz in einem spezifischen Feld nachzuweisen. Dadurch erhalten die Lernenden mehr Freiheit und zugleich auch mehr Verantwortung für ihr eigenes Lernen.

> Die Form des Kompetenznachweis wird individuell durch die Lernenden bestimmt.

#### Fachgespräch

In einem Fachgespräch begegnen sich Schülerin, Schüler und Lehrperson auf Augenhöhe. Die erworbene Kompetenz wird nachgewiesen, indem über einen fachlichen Gegenstand unter Anwendung der Fachsprache diskutiert wird.

Ein Fachgespräch ist kein *Referat*, bei welchem die Lernenden vorbereitete Inhalte präsentieren. Eine kurze, inhaltlich relevante Darlegung eines Unterrichtsgegenstandes kann jedoch eine gute Eröffnung des Fachgesprächs sein, wobei bereits nach kurzer Zeit eine Diskussion über kontoverse Inhalte, unklare Details oder angrenzende und verwande Themen entsteht.

#### Programmbeispiel

Anhand eines selbständig erarbeiteten Programmbeispiels können Lernende eine erworbene Kompetenz exemplarisch nachweisen. Dabei sollte das Programmbeispiel den relevanten Sachverhalt ins Zentrum stellen und auch Möglichkeiten bieten um Sonderfälle, mögliche Fehler oder Verbindungen zu anderen Kompetenzen zu illustrieren.

Programmbeispiele für den Nachweis von Kompetenzen sollten nie Codestellen enthalten, welche aus dem Internet, von anderen Lernenden oder weiteren Quellen ohne eine gründliche Reflexion übernommen wurden. Um eine erworbene Kompetenz erfolgreich nachzuweisen zu können, ist immer eine individuelle Bearbeitung des Problems bzw. des Programmbeispiels erforderlich.

#### Persönliche Zusammenfassung

Eine persönliche Zusammenfassung ist eine geeignete Form des Kompetenznachweis, wenn zum jeweiligen Unterrichtsgegenstand keine Materialien durch die Lehrperson zur Verfügung gestellt werden. Anhand einer persönlichen Zusammenfassung können Lernende die erlangte Kompetenz nachweisen, indem aus der persönlichen Zusammenfassung hervorgeht, dass alle relevanten Aspekte in angemessener fachlicher Breite und Tiefe abgebildet sind.

Die persönliche Zusammenfassung ist auf die fachlichen Inhalte reduziert und enthält eine Auflistung aller relevanter Quellen. Inhalte werden aus diesen Quellen nicht direkt und in grösseren Mengen übernommen. Um eine persönliche Zusammenfassung zu erstellen ist die individuelle Formulierung, Gliederung und Gewichtung der studierten Inhalte notwendig.

#### Eintrag im Lernjournal

Mit einem eintrag ins persönliche Lernjournal kann die angemessene Auseinandersetzung mit einem Thema aufgezeigt werden. Dabei ist erkennbar, in welcher Form und über welchen Zeitraum diese Auseinandersetzung erfolgt ist.

Im unterschied zur persönlichen Zusammenfassung ist der Eintrag im Lernjournal weniger auf eine vollständige Abbildung aller inhaltlich relevanten Aspekte fokussiert. Stattdessen stehen der individuelle Lernprozess, die persönliche Beschäftigung mit dem Thema, die Beziehung zu bereits vorhandenen Kenntnissen und Kompetenzen, eine Einbettung in die aktuelle oder möglicherweise folgende Arbeitstätigkeit sowie eine Einschätzung der fachlichen und/oder persönlichen Relevanz im Fokus.

#### Grafische Aufarbeitung

Für einige Kompetenzen eignet sich ein Kompetenznachweis in grafischer Form. Dabei wird das Wissen und die damit Verbundene Kompetenz in der Form einer Grafik zusammengefasst. Diese Grafik kann beispielsweise eine MindMap, ein UML-Diagramm, ein Struktogramm oder jede andere Art grafischer Darstellung sein.

Die grapfische Arbeitung sollte individuell angefertigt werden um die erworbene Kompetenz angemessen nachzuweisen.

### Bewertung

Im Rahmen der schulischen Ausbildung am GIBZ wird für jedes Modul eine Zeugnisnote erstellt. Während für andere Module die Notenbildung durch praktische oder theoretische Prüfungen, durch Projektarbeiten oder durch andere, vorgegebene Formen erfolgt, wird die Note für das Modul 319 anhand der erworbenen und nachgewiesenen Kompetenzen gebildet.

Die beiden Kompetenzstufen *Beginner* und *Intermediate* bilden zusammen das Fundament für die Programmiergrundlagen und damit für den weiteren Verlauf der schulischen sowie betrieblichen Ausbildung. Die entsprechenden Kompetenzen müssen daher von allen Lernenden im Rahmen des Modul 319 erworben werden um die Ausbildung erfolgreich fortführen zu können. Die Note 4.0 entspricht dem vollständigen Nachweis aller Kompetenzen auf diesen beiden ersten Kompetenzstufen.

> Das erwerben aller Kompetenzen auf den Kompetenzstufen *Beginner* und *Intermediate* entspricht der Note 4.0.

Der Nachweis jeder weiteren Kompetenz auf den Stufen *Advanced* und *Expert* führt zu einem vertieften Verständnis der Materie, zu einer gesteigerten Gesamtkompetenz der Lernenden und damit auch zu einem Anstieg der Note. Sofern alle Kompetenzen auf den beiden ersten Stufen *Beginner* und *Intermediate* nachgewiesen wurden, führt jeder Nachweis weiterer Kompetenzen zu einem Anstieg der Note um 0.125 Notenpunkte.

> Der Nachweis einer Kompetenz auf den Stufen *Advanced* und *Expert* führt zu einem Anstieg der Modulnote um 0.125.