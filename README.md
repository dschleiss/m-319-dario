# Modul 319 - Applikationen entwerfen und implementieren

In diesem Repository werden alle vorhandenen Unterlagen, Links und Hilfsmittel für das Modul 319 zur Verfügung gestellt. Die Inhalte werden fortlaufend ergänzt, aktualisiert und überarbeitet. Es wird deshalb *abgeraten*, ausschliesslich mit einen [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) dieses Repositories zu arbeiten.

## Schnellstart

Alle relevanten Informationen für die Arbeit mit diesem Repository im Rahmen des Modul 319 am GIBZ sind in verschiedenen Dokumenten enthalten. Um einen effizienten Einstieg zu gewährleisten, wird das sorgfältige Studium der nachfolgenden Dokumente bzw. Abschnitte empfohlen:
1. [Kompetenzenraster](01%20Kompetenzenraster/Kompetenzenraster.md): Es reicht, die tabellarische Darstellung der Kompetenzen zu überfliegen und nur die beiden Abschnitte *Kompetenzbereiche* und *Kompetenzstufen* genau zu lesen und zu verstehen.
2. [Arbeit mit dem Kompetenzenraster](01%20Kompetenzenraster/Arbeit%20mit%20dem%20Kompetenzenraster.md): Ganzes Dokument sorgfältig studieren.
3. [Inputs](00%20Organisatorisches/Inputs.md): Die allgemeinen Informationen (ohne *Zeitplan*) lesen und allfällige Fragen klären.
4. [Lernjournal](00%20Organisatorisches/Lernjournal.md): Das ganze Dokument ist relevant - genau lesen und Fragen klären.
5. [Technische Informationen](00%20Organisatorisches/Technisches.md) zur Wahl und Installation von Werkzeugen finden Sie im Dokument [Technisches.md](00%20Organisatorisches/Technisches.md). Dieses Dokument kann gut auch zu einem späteren Zeitpunkt studiert werden.

## Externe Links
- Offizielle [Modulidentifikation](https://www.modulbaukasten.ch/modul/c6b67ea8-5e85-eb11-a812-0022486f644f) des Modul 319
- [Moodle](https://moodle-gibz.ch/course/view.php?id=60) für die Verwaltung der erreichten Kompetenzen